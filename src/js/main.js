$(document).ready(function(){
    //Menu
    /* Hamburger menu animation */
    $('.header-wrap__toogler').click(function(){
        $(this).toggleClass('open');
    });
    /* Menu fade/in out on mobile */
    $(".header-wrap__toogler").click(function(e){
        e.preventDefault();
        $(".header-menu").toggleClass('open');
        $("body").toggleClass('lock')
    });

    //Select menu function placeholder
    $.widget( 'app.selectmenu', $.ui.selectmenu, {
        _drawButton: function () {
            this._super();
            let selected = this.element
                    .find( '[selected]' )
                    .length,
                placeholder = this.options.placeholder;

            if (!selected && placeholder) {
                this.buttonItem.text(placeholder);
            }
        }
    })
    //Datepicker function, custom class function
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd.mm.yy',
            beforeShow: function(input, inst) {
                inst.dpDiv
                    .removeClass('datepicker-home datepicker-property datepicker-room')
                    .addClass($(input).data('dp-class'));
            }
        });
    }());
    //Select menu function
    $( "#yourRoom" ).selectmenu({
        placeholder: 'Choose your room'
    });
    $( "#yourProperty" ).selectmenu({
        placeholder: 'Choose Propery'
    });
    $( "select" ).selectmenu();
    //LightGallery
    // $("#gallery").lightGallery();
    //Masonry on JS
    var msnry = new Masonry( '.gallery-wrap__item', {
        itemSelector: '.gallery-wrap__item-link',
        gutter: 0,
        columnWidth : '.grid-sizer',
        percentPosition : 'true',
        fitWidth: true,
    });
});
/*$(window).load(function() {
    $('.gallery-wrap__item').masonry({
        itemSelector: 'gallery-wrap__item-link',
        columnWidth: 340,
        gutter: 20
    });
});*/
